package com.henry.html;

import java.util.List;

import lombok.Data;

@Data
public class HEAD {
	private String title;
	private String description;
	private List<LINK> links;
}
