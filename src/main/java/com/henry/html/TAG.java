package com.henry.html;

import java.util.List;

import lombok.Data;

@Data
public class TAG {
	String name;
	List<String> classes;
	List<TAG> children;
}
