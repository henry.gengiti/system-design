package com.henry.html;

import java.util.ArrayList;
import java.util.Arrays;

public class App 
{
	
    public static void main( String[] args )
    {
    	HTML html = new HTML();
    	
    	HEAD head = new HEAD();
    	head.setDescription("my test website");
    	head.setTitle("my test title");
    	head.setLinks(new ArrayList<>());
		html.head = head ;
    	
    	BODY body = new BODY();
    	
    	TAG tag =  new TAG();
    	tag.setName("DIV");
    	tag.setClasses(Arrays.asList("class1", "class2", "class3"));
		body.addTags(tag);
    	
		html.body = body;
	
		System.out.println("html" + html);
		
    }
}
