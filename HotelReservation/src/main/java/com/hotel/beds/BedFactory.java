package com.hotel.beds;

public class BedFactory {
   
	public iBed getBed(String s) {
		
		if(s.equals("King"))
			return new KingBed();
		else if (s.equals("Queen"))
			return new QueenBed();
		else if (s.equals("Twin"))
			return new TwinBed();
		else 
			throw new IllegalArgumentException("invalid input");
	}
}
