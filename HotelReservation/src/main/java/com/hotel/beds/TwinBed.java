package com.hotel.beds;

public class TwinBed implements iBed {

	public String getName() {
		return "Twin bed";
	}

	public double getPrice() {
		return 50;
	}

}
