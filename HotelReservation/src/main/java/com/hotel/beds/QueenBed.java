package com.hotel.beds;

public class QueenBed implements iBed {

	public String getName() {
		return "Queen bed";
	}

	public double getPrice() {
		return 70;
	}

}
