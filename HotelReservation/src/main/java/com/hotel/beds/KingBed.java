package com.hotel.beds;

public class KingBed implements iBed {

	public String getName() {
		return "King bed";
	}

	public double getPrice() {
		return 100;
	}

}
