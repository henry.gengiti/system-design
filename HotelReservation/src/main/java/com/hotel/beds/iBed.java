package com.hotel.beds;

public interface iBed {
   String getName();
   double getPrice();
}
