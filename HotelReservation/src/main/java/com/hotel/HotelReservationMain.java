package com.hotel;

import com.hotel.beds.BedFactory;
import com.hotel.beds.iBed;
import com.hotel.booking.BookHotelRoom;
import com.hotel.booking.BookHotelRoomInventory;
import com.hotel.room.Room;
import com.hotel.room.RoomInventory;
import com.hotel.room.iRoom;

public class HotelReservationMain {

	public static void main(String[] args) {

		BedFactory bf = new BedFactory();

		iBed king = bf.getBed("King");
		iBed queen = bf.getBed("Queen");
		iBed twin = bf.getBed("Twin");
		
		iRoom kingRoom = new Room(king);
		iRoom queenRoom = new Room(queen);
		iRoom twinRoom = new Room(twin);
		iRoom kingRoomAndTwin = new Room(king).addBeds(twin);
		iRoom queenRoomAndTwin = new Room(queen).addBeds(twin);
		iRoom twinRoomAndTwin = new Room(twin).addBeds(twin);

		System.out.println("Start adding rooms to room inventory");
		
		RoomInventory roomInventory = new RoomInventory();
		kingRoom = roomInventory.addRooms(kingRoom);
		queenRoom = roomInventory.addRooms(queenRoom);
		twinRoom = roomInventory.addRooms(twinRoom);
		kingRoomAndTwin = roomInventory.addRooms(kingRoomAndTwin);
		queenRoomAndTwin = roomInventory.addRooms(queenRoomAndTwin);
		twinRoomAndTwin = roomInventory.addRooms(twinRoomAndTwin);
		
		System.out.println("showing available rooms");
		roomInventory.showUnBookedRooms();
		
		BookHotelRoomInventory orderInventory = new BookHotelRoomInventory();
		 
		BookHotelRoom bookHotelRoom1 = new BookHotelRoom(kingRoom);
		 
		System.out.println("Going to book room # " + kingRoom.getId());
		bookHotelRoom1 = orderInventory.addOrder(bookHotelRoom1);
		 
		System.out.println("showing booked orders");
		orderInventory.showOrders();
		
		System.out.println("showing available rooms for booking");
		roomInventory.showUnBookedRooms();
		
		System.out.println("showing rooms already booked");
		roomInventory.showBookedRooms();
		
		System.out.println("checking in room # " + bookHotelRoom1.getRoom().getId() + " and order # " + bookHotelRoom1.getOrderId());
		bookHotelRoom1.getRoom().checkin();
		
		System.out.println("showing checked in rooms");
		roomInventory.showCheckedInRooms();
		
		System.out.println("closing the order aka checked in rooms");
		bookHotelRoom1.close();
		
		System.out.println("showing available rooms");
		roomInventory.showUnBookedRooms();
		
	}
}
