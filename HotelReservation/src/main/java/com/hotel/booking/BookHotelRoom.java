package com.hotel.booking;

import com.hotel.room.iRoom;

public class BookHotelRoom {

	int id;
	iRoom room;
	BookingStatus status;
	
	public BookHotelRoom(iRoom room) {
		this.room = room;
		status = BookingStatus.OPEN;	
	}
	
	public void setOrderId(int id) {
		this.id = id;
	}
	
	public int getOrderId() {
		return id;
	}
	
	public iRoom getRoom() {
		return this.room;
	}
	
    public void close() {
    	System.out.println("\t going to checkout room #" + this.room.getId());
    	room.checkout();
    	status = BookingStatus.CLOSE;
	}
	
}
