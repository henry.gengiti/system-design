package com.hotel.booking;

import java.util.HashMap;
import java.util.Map;

import com.hotel.room.iRoom;

public class BookHotelRoomInventory {
	Map<Integer, BookHotelRoom> order = new HashMap<>();
	
	public BookHotelRoom addOrder(BookHotelRoom bookHotelRoom) {
		
		int id = order.size() + 1;
		bookHotelRoom.setOrderId(id);
		
		iRoom room = bookHotelRoom.getRoom();
		room.setIsBooked(true);
		
		order.put(bookHotelRoom.getOrderId(), bookHotelRoom);
		return order.get(bookHotelRoom.getOrderId());
	}
	
	public BookHotelRoom getOrder(Integer id) {
		return order.get(id);
	}
	
	public void showOrders () {
		order.entrySet().forEach(k -> 
			System.out.println("\t order id " + k.getKey() + ", room id " + k.getValue().room.getId())
		);
	}
	
	public BookHotelRoom deleteOrder(BookHotelRoom bookHotelRoom) {
		return order.remove(bookHotelRoom.id);
	}
	
}
