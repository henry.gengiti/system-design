package com.hotel.room;

import java.util.HashMap;
import java.util.Map;

public class RoomInventory {
	Map<Integer, iRoom> rooms = new HashMap<>();

	public iRoom addRooms(iRoom room) {
		int id = rooms.size() + 1;
		room.setId(id);

		rooms.put(id, room);
		return room;
	}

	public void removeRooms(Room room) {
		rooms.remove(room.id);
	}

	public void showUnBookedRooms() {
		rooms.entrySet().stream().map(x -> x.getValue()).filter(x -> !x.isBooked()).forEach(
				room -> System.out.println("\t Open : room id " + room.getId() + ", price: " + room.getPrice()));

	}

	public void showBookedRooms() {
		rooms.entrySet().stream().map(x -> x.getValue()).filter(x -> x.isBooked()).forEach(room -> System.out
				.println("\t Booked:  booked room id " + room.getId() + " price: " + room.getPrice()));

	}

	public void showCheckedInRooms() {
		rooms.entrySet().stream().map(x -> x.getValue()).filter(x -> x.getStatus() == RoomStatus.CHECKED_ID).forEach(
				room -> System.out.println("\t Checked In room id " + room.getId() + " price: " + room.getPrice()));

	}

	public void showCheckedOutRooms() {
		rooms.entrySet().stream().map(x -> x.getValue()).filter(x -> x.getStatus() == RoomStatus.CHECKOUT_OUT).forEach(
				room -> System.out.println("\t Checked Out room id " + room.getId() + " price: " + room.getPrice()));

	}
}
