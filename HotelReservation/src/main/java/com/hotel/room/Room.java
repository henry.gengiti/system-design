package com.hotel.room;

import java.util.ArrayList;
import java.util.List;

import com.hotel.beds.iBed;

public class Room implements iRoom {

	List<iBed> beds;
	RoomStatus status;
	Integer id;
	double price;
	boolean isBooked;
	
	public Room(iBed bed) {
		beds = new ArrayList<>();
		this.beds.add(bed);
		this.status = RoomStatus.AVAILABLE;
	}

	public double getPrice() {
		double addedPrice = 0;
		for(iBed bed: beds) {
			addedPrice = addedPrice + bed.getPrice();
		}
		
		return price  + addedPrice;
	}

	public RoomStatus getStatus() {
		return status;
	}
	
	public void setAsAvailable() {
		status = RoomStatus.AVAILABLE;
	}
	
	public Room addBeds(iBed bed) {
		beds.add(bed);
		return this;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void checkin() {
		this.status = RoomStatus.CHECKED_ID;
	}

	@Override
	public void checkout() {
		this.status = RoomStatus.CHECKOUT_OUT;
		this.isBooked = false;
	}

	@Override
	public void showBeds() {
		 this.beds.forEach(x -> System.out.println("bed - " +x));
	}

	@Override
	public void setIsBooked(boolean val) {
		this.isBooked = val;
	}

	@Override
	public boolean isBooked() {
		return isBooked;
	}

}
