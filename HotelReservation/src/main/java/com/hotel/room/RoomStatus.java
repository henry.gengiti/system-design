package com.hotel.room;

public enum RoomStatus {
 AVAILABLE, CHECKED_ID, CHECKOUT_OUT;
}
