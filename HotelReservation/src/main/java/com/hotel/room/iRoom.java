package com.hotel.room;

import com.hotel.beds.iBed;

public interface iRoom {
  public void setId(Integer id);
  public int getId();
  
  public double getPrice();
  
  public RoomStatus getStatus();
  public void setAsAvailable();
  
  public iRoom addBeds(iBed bed);
  public void showBeds();
  
  public boolean isBooked();
  public void setIsBooked(boolean val);
  
  public void checkin();
  public void checkout();
  
  

}
