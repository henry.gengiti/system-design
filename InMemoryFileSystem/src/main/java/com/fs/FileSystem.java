package com.fs;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FileSystem {

	Directory matchedDirectory = null;

	class FileAndPath {
		String fileName;
		String extension;
		String path;
	}

	Directory directory = new Directory();

	FileSystem() {

		directory.setCurrentPath("/");
		directory.setDirectories(new HashSet<>());
		directory.setFiles(new HashSet<>());
		directory.setName("root");
		directory.setParentPath("");
		directory.setPathHash("/".hashCode());

	}

	public List<String> ls(String path) {

		matchedDirectory = null;
		getMatchedDirectory(path.hashCode(), directory);

		if (matchedDirectory == null) {
			return new ArrayList<>();
		}

		List<String> files = matchedDirectory.getFiles().stream().map(x -> x.getName()).collect(Collectors.toList());
		List<String> directories = matchedDirectory.getDirectories().stream().map(x -> x.getName())
				.collect(Collectors.toList());

		files.addAll(directories);

		return files;
	}

	public void mkdir(String path) {

		if (!path.contains("/")) {
			throw new IllegalArgumentException("path must contain /");
		}

		matchedDirectory = null;

		String otherThanNewFolder = path.substring(0, path.lastIndexOf('/'));
		String newFolder = path.substring(path.lastIndexOf('/') + 1, path.length());

		if (otherThanNewFolder.equals("")) {
			// this is root
			matchedDirectory = directory;
		} else {
			getMatchedDirectory(otherThanNewFolder.hashCode(), directory);
		}

		if (matchedDirectory == null) {
			throw new IllegalArgumentException("invalid path");
		}

		if (matchedDirectory.getDirectories().stream().anyMatch(x -> x.getPathHash() == path.hashCode())) {
			throw new RuntimeException("path already exists");
		}

		Directory dir = new Directory();
		dir.setCurrentPath(path);
		dir.setName(newFolder);
		dir.setPathHash(path.hashCode());
		dir.setDirectories(new HashSet<>());
		dir.setFiles(new HashSet<>());

		matchedDirectory.getDirectories().add(dir);
	}

	public void addContentToFile(String path, String content) {

		Optional<File> matchedFile = getContentFromFile(path);

		// check if the same file already exists
		if (matchedFile.isPresent()) {
			StringBuilder sb = new StringBuilder();
			sb.append(matchedFile.get().getContent());
			sb.append(content);

			matchedFile.get().setContent(sb.toString());
			matchedFile.get().setLastModifiedDate(LocalDateTime.now());
		} else {
			FileAndPath fap = extractFileAndPath(path);
			
			File file = new File();
			file.setContent(content);
			file.setCreatedDate(LocalDateTime.now());
			file.setLastModifiedDate(LocalDateTime.now());
			file.setName(fap.fileName);

			matchedDirectory.getFiles().add(file);
		}

	}

	public Optional<File> getContentFromFile(String path) {
		FileAndPath fap = extractFileAndPath(path);

		matchedDirectory = null;

		getMatchedDirectory(fap.path.hashCode(), directory);

		if (matchedDirectory == null) {
			throw new IllegalArgumentException("invalid path");
		}

		Optional<File> matchedfile = matchedDirectory.getFiles().stream().filter(x -> x.getName().equals(fap.fileName))
				.findFirst();

		return matchedfile;

	}

	private FileAndPath extractFileAndPath(String path) {
		String[] paths = path.split("/");

		String fileNameExtension = paths[paths.length - 1];

		if (!fileNameExtension.contains(".")) {
			throw new IllegalArgumentException("path does not contain a file");
		}

		FileAndPath fap = new FileAndPath();

		fap.path = path.substring(0, path.lastIndexOf("/"));

		String[] fileNameExtSplit = fileNameExtension.split("\\.");
		fap.fileName = fileNameExtSplit[0];
		fap.extension = fileNameExtSplit[1];

		return fap;
	}

	private void getMatchedDirectory(int pathHashCode, Directory directory) { // "/", "dir1"

		if (directory.getPathHash() == pathHashCode) {
			matchedDirectory = directory;
		}

		directory.getDirectories().forEach(dir -> {
			getMatchedDirectory(pathHashCode, dir);
		});

	}

}
