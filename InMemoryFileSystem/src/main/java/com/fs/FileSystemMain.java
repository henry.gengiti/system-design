package com.fs;

public class FileSystemMain {

	public static void main(String[] args) {

		FileSystem fs = new FileSystem();
		fs.mkdir("/x");
		fs.mkdir("/y");
		fs.mkdir("/z");
		fs.mkdir("/x/x2");
		fs.mkdir("/x/x3");
		fs.mkdir("/x/x4");
		fs.mkdir("/x/x5");
		fs.mkdir("/x/x6");
		
		System.out.println("printing all objects in the root");
		fs.ls("/").forEach(x -> System.out.println(x));
		
		System.out.println("printing all objects in the sub directory /x");
		fs.ls("/x").forEach(x -> System.out.println(x));
		
		fs.addContentToFile("/x/myfile.doc", "hello how are you");
		System.out.println(fs.getContentFromFile("/x/myfile.doc").get().getContent());
		
		fs.addContentToFile("/x/myfile.doc", ". and I hope you are doing good.");
		System.out.println(fs.getContentFromFile("/x/myfile.doc").get().getContent());
	}
}
