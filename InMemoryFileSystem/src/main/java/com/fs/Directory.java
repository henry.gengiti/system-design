package com.fs;

import java.util.Set;

public class Directory {
	int pathHash;
	String currentPath;
	String parentPath;
	String name;
	
	Set<File> files;
	Set<Directory> directories;
	
	public int getPathHash() {
		return pathHash;
	}

	public void setPathHash(int pathHash) {
		this.pathHash = pathHash;
	}

	
	
	

	public String getCurrentPath() {
		return currentPath;
	}

	public void setCurrentPath(String currentPath) {
		this.currentPath = currentPath;
	}

	public String getParentPath() {
		return parentPath;
	}

	public Set<File> getFiles() {
		return files;
	}

	public void setFiles(Set<File> files) {
		this.files = files;
	}

	public Set<Directory> getDirectories() {
		return directories;
	}

	public void setDirectories(Set<Directory> directories) {
		this.directories = directories;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	
}
