package com.henry.card_deck;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Card {

	private Suit suit;
	private Integer faceValue;

}
