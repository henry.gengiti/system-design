package com.henry.card_deck;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CardDeck {

	List<Card> cards = new ArrayList<>();

	CardDeck() {

		for (int i = 1; i <= 13; i++) {

			for (Suit s : Suit.values()) {
				cards.add(new Card(s, i));
			}
		}
	}

	List<Card> getCardByStuit(Suit suit) {

		return cards.stream().filter(x -> x.getSuit() == suit).collect(Collectors.toList());

	}

}
